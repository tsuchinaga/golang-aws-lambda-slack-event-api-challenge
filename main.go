package main

import (
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"net/http"
)

type SlackChallengeRequest struct {
	Token     string
	Challenge string
	Type      string
}

func Challenge(request events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	req := new(SlackChallengeRequest)
	err := json.Unmarshal([]byte(request.Body), req)
	if err != nil {
		return nil, err
	}

	return &events.APIGatewayProxyResponse{
		StatusCode: http.StatusOK,
		Body:       req.Challenge,
	}, nil
}

func main() {
	lambda.Start(Challenge)
}
