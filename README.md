### Golang AWS Lambda Slack Event Api Challenge

Golangを使ったAWS Lambdaで、Slack Event APIのChallengeをする

## 環境

* Windows 10
* Golang 1.12

## Buildコマンド

```bash
$ GOOS=linux go build -o SlackChallenge main.go
$ build-lambda-zip.exe -o SlackChallenge.zip SlackChallenge
```
